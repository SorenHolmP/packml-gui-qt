#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _state_machine          = new QStateMachine(this);

    _state_unholding        = new QState();
    _state_held             = new QState();
    _state_holding          = new QState();
    _state_idle             = new QState();
    _state_starting         = new QState();
    _state_execute          = new QState();
    _state_completing       = new QState();
    _state_complete         = new QState();
    _state_resetting        = new QState();
    _state_unsuspending     = new QState();
    _state_suspended        = new QState();
    _state_suspending       = new QState();
    _state_stopped          = new QState();
    _state_stopping         = new QState();
    _state_clearing         = new QState();
    _state_aborted          = new QState();
    _state_aborting         = new QState();


    _packML_states = {_state_unholding, _state_held, _state_holding,_state_idle, _state_starting, _state_execute,
                      _state_completing, _state_complete,_state_resetting, _state_unsuspending, _state_suspended,
                      _state_suspending, _state_stopped, _state_stopping, _state_clearing, _state_aborted, _state_aborting};


    _top_box_states = {_state_unholding, _state_held, _state_holding,_state_idle, _state_starting, _state_execute, _state_complete,
                       _state_completing, _state_resetting, _state_unsuspending, _state_suspended, _state_suspending};

    _active_states = {_state_unholding, _state_holding, _state_starting, _state_execute, _state_completing,
                     _state_resetting, _state_unsuspending, _state_suspending, _state_stopping,
                     _state_clearing, _state_aborting};


    for(QState* &state : _top_box_states)
    {
        state->addTransition(ui->abortButton,SIGNAL(clicked(bool)),_state_aborting);
        state->addTransition(ui->stopButton,SIGNAL(clicked(bool)),_state_stopping);
    }

    _state_unholding->addTransition(ui->stateCompleteButton,SIGNAL(clicked(bool)),_state_execute);
    _state_held->addTransition(ui->unHoldButton, SIGNAL(clicked(bool)), _state_unholding);
    _state_holding->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_held);
    _state_idle->addTransition(ui->startButton, SIGNAL(clicked(bool)), _state_starting);
    _state_starting->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_execute);
    _state_execute->addTransition(ui->holdButton, SIGNAL(clicked(bool)), _state_holding);
    _state_execute->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_completing);
    _state_execute->addTransition(ui->suspendButton, SIGNAL(clicked(bool)), _state_suspending);
    _state_completing->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_complete);
    _state_complete->addTransition(ui->resetButton, SIGNAL(clicked(bool)), _state_resetting);
    _state_resetting->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_idle);
    _state_unsuspending->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_execute);
    _state_suspended->addTransition(ui->unsuspendButton, SIGNAL(clicked(bool)), _state_unsuspending);
    _state_suspending->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_suspended);
    _state_stopped->addTransition(ui->resetButton, SIGNAL(clicked(bool)), _state_resetting);
    _state_stopping->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_stopped);
    _state_clearing->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_stopped);
    _state_aborted->addTransition(ui->clearButton, SIGNAL(clicked(bool)), _state_clearing);
    _state_aborting->addTransition(ui->stateCompleteButton, SIGNAL(clicked(bool)), _state_aborted);


    _state_unholding->assignProperty(ui->stateLabel,"text","Unholding");
    _state_held->assignProperty(ui->stateLabel,"text","Held");
    _state_holding->assignProperty(ui->stateLabel,"text","Holding");
    _state_idle->assignProperty(ui->stateLabel,"text","Idle");
    _state_starting->assignProperty(ui->stateLabel,"text","Starting");
    _state_execute->assignProperty(ui->stateLabel,"text","Execute");
    _state_completing->assignProperty(ui->stateLabel,"text","Completing");
    _state_complete->assignProperty(ui->stateLabel,"text","Complete");
    _state_resetting->assignProperty(ui->stateLabel,"text","Resetting");
    _state_unsuspending->assignProperty(ui->stateLabel,"text","Unsuspending");
    _state_suspended->assignProperty(ui->stateLabel,"text","Suspended");
    _state_suspending->assignProperty(ui->stateLabel,"text","Suspending");
    _state_stopped->assignProperty(ui->stateLabel,"text","Stopped");
    _state_stopping->assignProperty(ui->stateLabel,"text","Stopping");
    _state_clearing->assignProperty(ui->stateLabel,"text","Clearing");
    _state_aborted->assignProperty(ui->stateLabel,"text","Aborted");
    _state_aborting->assignProperty(ui->stateLabel,"text","Aborting");


    for(QState* &state : _packML_states)
        _state_machine->addState(state);


    _state_machine->setInitialState(_state_idle);
    _state_machine->start();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerSlot()
{
    qDebug() << "tick...";
}
